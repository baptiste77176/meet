<?php

namespace App\Repository;

use App\Entity\UserLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserLink[]    findAll()
 * @method UserLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserLinkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserLink::class);
    }

    // /**
    //  * @return UserLink[] Returns an array of UserLink objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserLink
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
