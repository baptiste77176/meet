<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserInfos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * UserRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function setIsConnectedToTrue($userId)
    {
        return $this->createQueryBuilder('user')
            ->update(User::class, 'user')
            ->set('user.isConnected', '?1')
            ->setParameter(1, TRUE)
            ->where('user.id = :paramuserId')
            ->setParameter('paramuserId', $userId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function setIsConnectedToFalse($userId)
    {
        return $this->createQueryBuilder('user')
            ->update(User::class, 'user')
            ->set('user.isConnected', '?1')
            ->setParameter(1, FALSE)
            ->where('user.id = :paramuserId')
            ->setParameter('paramuserId', $userId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param int $userId
     * @return mixed
     * @throws \Exception
     */
    public function setIsLastLoginAt(int $userId)
    {
        return $this->createQueryBuilder('user')
            ->update(User::class, 'user')
            ->set('user.lastLoginAt', '?1')
            ->setParameter(1, new \DateTime())
            ->where('user.id = :paramuserId')
            ->setParameter('paramuserId', $userId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param float $lat
     * @param float $lng
     * @param User $user
     * @return mixed
     */
    public function setLatAndLng(float $lat, float $lng, User $user)
    {
        return $this->createQueryBuilder('user')
            ->update(User::class, 'user')
            ->set('user.lat', ':paramLat')
            ->setParameter('paramLat', $lat)
            ->set('user.lng', ':paramLng')
            ->setParameter('paramLng', $lng)
            ->where('user = :paramuser')
            ->setParameter('paramuser', $user)
            ->getQuery()
            ->execute();
    }

    /**
     * @param User $user
     * @param string $sexType
     * @param string $sexFilter
     * @param array|null $dislikedId
     * @param array|null $likeSend
     * @param array|null $currentHaveMatched
     * @param string|null $interestFilter
     * @param string|null $lifestyleFilter
     * @param string|null $charactersFilter
     * @return mixed
     */
    public function displayUserForMatch(
        User $user,
        string $sexType,
        string $sexFilter,
        array $dislikedId = null,
        array $likeSend = null,
        array $currentHaveMatched = null,
        ?string $interestFilter = null,
        ?string $lifestyleFilter = null,
        ?string $charactersFilter = null
    )
    {
        $qb = $this->createQueryBuilder('user')
            ->Join('user.Infos', 'infos', 'with', 'user != :param')
            ->setParameter('param', $user)
            ->where('user.isFirstConnection = 1')
            ->andWhere('infos.herefor = :paramHereFor')
            ->setParameter('paramHereFor', $sexType)
            ->andWhere('user.sex = :paramSex')
            ->setParameter('paramSex', $sexFilter)
            ->orderBy('RAND()');

        if (isset($dislikedId)) {
            $qb->andWhere('user.id NOT IN(:dislike)');
            $qb->setParameter('dislike', $dislikedId);
        }

        if (isset($likeSend)) {
            $qb->andWhere('user.id NOT IN(:likeSend)');
            $qb->setParameter('likeSend', $likeSend);
        }

        if (isset($currentHaveMatched)) {
            $qb->andWhere('user.id NOT IN(:matched)');
            $qb->setParameter('matched', $currentHaveMatched);
        }

        if ($sexType === 'femme') {

            if (isset($interestFilter)) {
                $qb->andWhere('infos.interest = :paramInterest');
                $qb->setParameter('paramInterest', $interestFilter);
            }
            if (isset($lifestyleFilter)) {
                $qb->andWhere('infos.lifestyle = :paramLifestyle');
                $qb->setParameter('paramLifestyle', $lifestyleFilter);
            }
            if (isset($charactersFilter)) {
                $qb->andWhere('infos.characters = :paramCharacters');
                $qb->setParameter('paramCharacters', $charactersFilter);
            }
        }
        return $qb->getQuery()->getResult();
    }
}
