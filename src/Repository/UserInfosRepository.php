<?php

namespace App\Repository;

use App\Entity\UserInfos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserInfos|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInfos|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserInfos[]    findAll()
 * @method UserInfos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserInfosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserInfos::class);
    }
}
