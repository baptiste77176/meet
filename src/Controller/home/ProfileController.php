<?php

namespace App\Controller\home;

use App\Form\ProfilePictureType;
use App\Form\UserInfosMenType;
use App\Form\UserInfosWomenType;
use App\Repository\UserRepository;
use App\services\LikeLibrary;
use App\services\StatLibrary;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/home")
 */
class ProfileController extends AbstractController
{
    /**
     *
     * Display the profile for the current user
     *
     * @Route("/profile", name="profile")
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index(): Response
    {
        $user = $this->getUser();
        $arrayImg = explode(",", $user->getSlider()->getImg());
        return $this->render('home/profile/profile.html.twig', [
            'user' => $user,
            'arrayImg' => $arrayImg
        ]);
    }

    /**
     *
     * Update current user info or profile picture
     *
     * @Route("/edit_profile", name="edit_profile")
     * @param Request $request
     * @param SessionInterface $session
     * @param StatLibrary $statLibrary
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function editProfile(
        Request $request,
        SessionInterface $session,
        StatLibrary $statLibrary): Response
    {
        $user = $this->getUser();

        $typeImg = ProfilePictureType::class;
        $formImg = $this->createForm($typeImg, $user);
        $formImg->handleRequest($request);

        if (!$session->has('ProfilePicture')) {
            $session->set('ProfilePicture', $user->getProfilePicture());
        }

        $tmp = $session->get('ProfilePicture');
        if ($formImg->isSubmitted() && $formImg->isValid()) {

            $dir = getcwd() . '/img/' . $user->getPseudo() . '/Profile';
            $file = $formImg->get('ProfilePicture')->getData();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            try {
                $file->move($dir, $fileName);
            } catch (FileException $e) {
                $e->getMessage();
            }
            $user->setprofilepicture($fileName);

            $statLibrary->hydrateProfilePictureChange($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            if (isset($file) && !empty($tmp) && ($tmp !== $fileName)) {
                unlink(getcwd() . '/img/' . $user->getPseudo() . '/Profile/' . $tmp);
            }
            $session->remove('ProfilePicture');
            $this->addFlash(
                'editPicture',
                'Your picture have changed'
            );
            return $this->redirectToRoute('profile');
        }

        if ($user->getSex() === 'homme') {
            $typeInfos = UserInfosMenType::class;
        } else {
            $typeInfos = UserInfosWomenType::class;
        }

        $formInfos = $this->createForm($typeInfos, $user->getInfos());
        $formInfos->handleRequest($request);

        if ($formInfos->isSubmitted() && $formInfos->isValid()) {

            $data = $formInfos->getData();
            $data->setCity(ucfirst(strtolower($data->getCity())));
            $entityManager = $this->getDoctrine()->getManager();
            $user->getRating()->setDomProfileType($data->getProfileDominance());

            $entityManager->persist($user);
            $entityManager->persist($data);
            $entityManager->flush();

            $this->addFlash(
                'editProfile',
                'Your profile is edited'
            );
            return $this->redirectToRoute('profile');
        }

        return $this->render('home/profile/edit_profile.html.twig', [
            'formImg' => $formImg->createView(),
            'formInfos' => $formInfos->createView(),
            'user' => $user,
        ]);
    }


    /**
     * @Route("/remove", name="remove_profile_picture")
     * @param SessionInterface $session
     * @return RedirectResponse
     */
    public function removeProfilePicture(SessionInterface $session): RedirectResponse
    {
        $user = $this->getUser();
        $user->setProfilepicture(null);
        if ($session->has('ProfilePicture')) {
            unlink(getcwd() . '/img/' . $user->getPseudo() . '/Profile/' . $session->get('ProfilePicture'));
            $session->remove('ProfilePicture');
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('profile');
    }


    /**
     * @Route("/userDisplay/{hash}/{id}", name="distant_user_display")
     * @param UserRepository $userRepository
     * @param LikeLibrary $likeLibrary
     * @param int $id
     * @param StatLibrary $statLibrary
     * @return Response
     * @throws \Exception
     */
    public function distantUserDisplay(UserRepository $userRepository, LikeLibrary $likeLibrary, int $id, StatLibrary $statLibrary): Response
    {
        $user = $this->getUser();
        $distantUser = $userRepository->findOneBy(['id' => intval($id)]);

        // definie si on affiche les bouttons like/dislike
        $interactionCheck = true;
        $distantLikeReceivedArray = explode(',', $distantUser->getMatching()->getLikeReceived());
        $currentDislikeGivedArray = explode(',', $user->getMatching()->getDislikeSend());
        $currentHaveMatchedArray = explode(',', $user->getMatching()->getHaveMatched());
        if (in_array($user->getId(), $distantLikeReceivedArray) ||
            in_array($distantUser->getId(), $currentDislikeGivedArray) ||
            in_array($distantUser->getId(), $currentHaveMatchedArray)
        ) {
            $interactionCheck = false;
        }

        $statLibrary->hydrateVisiteHandler($user, $distantUser);
        $stopLike = $likeLibrary->likeTimer($user);

        return $this->render('home/profile/profile.html.twig', [
            'interactionCheck' => $interactionCheck,
            'checkIsDistantUser' => 1,
            'user' => $distantUser,
            'arrayImg' => explode(",", $distantUser->getSlider()->getImg()),
            'stopLike' => $stopLike
        ]);
    }
}
