<?php

namespace App\Controller\home;

use App\Form\SliderType;
use App\Repository\SliderRepository;
use App\services\StatLibrary;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home")
 */
class SliderController extends AbstractController
{
    /**
     * @Route("/slider", name="slider")
     * @param Request $request
     * @param SliderRepository $sliderRepository
     * @param StatLibrary $statLibrary
     * @return Response
     */
    public function index(Request $request, SliderRepository $sliderRepository, StatLibrary $statLibrary): Response
    {
        $user = $this->getUser();
        $sliderEntity = $sliderRepository->findOneBy(['id' => $user->getId()]);
        $form = $this->createForm(SliderType::class, $sliderEntity);
        $arrayTmp = explode(",", $sliderEntity->getImg());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && count($arrayTmp) < 12) {
            $data = $form->getData();
            $file = $form->get('img')->getData();
            if (!empty($file) && isset($file) && $file !== null) {

                $dir = getcwd() . '/img/' . $user->getPseudo() . '/Slider';
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                array_push($arrayTmp, $fileName);
                $stringTmp = implode(',', $arrayTmp);

                $data->setImg($stringTmp);
                $statLibrary->hydrateSLiderAdd($user);

                $em = $this->getDoctrine()->getManager();
                $em->persist($sliderEntity);
                $em->flush();

                try {
                    $file->move($dir, $fileName);
                } catch (FileException $e) {
                    $e->getMessage();
                }
            }
            return $this->redirectToRoute('slider');
        } else {
            if (count($arrayTmp) < 11) {
                $errors = "";
            } else {
                $errors = "Vous avez atteint le nombre maximum  de photos a afficher";
            }
            $tmp = $sliderRepository->find(['id' => $user->getId()]);
            $arrayImg = explode(",", $tmp->getImg());

            return $this->render('home/slider/slider.html.twig', [
                'form' => $form->createView(),
                'arrayImg' => $arrayImg,
                'pseudo' => $user->getPseudo(),
                'errors' => $errors
            ]);
        }
    }

    /**
     *  @Route("/Slider/{img}", name="slider_remove")
     * @param SliderRepository $sliderRepository
     * @param string $img
     * @return Response
     */
    public function removeAction(SliderRepository $sliderRepository,string $img): Response
    {
        $sliderEntity = $sliderRepository->findOneBy(['id' => $this->getUser()->getId()]);
        $arrayTmp = explode(",", $sliderEntity->getImg());

        foreach ($arrayTmp as $key => $value) {
            if ($img === $value) {
                unset($arrayTmp[$key]);
            }
        }
        $stringTmp = implode(',', $arrayTmp);
        $sliderEntity->setImg($stringTmp);
        $em = $this->getDoctrine()->getManager();
        $em->persist($sliderEntity);
        $em->flush();
        unlink(getcwd() . '/img/' . $this->getUser()->getPseudo() . '/Slider/' . $img);
        return $this->redirectToRoute('slider');
    }
}
