<?php

namespace App\Controller\home;

use App\Form\UserInfosComplementaryType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class HomePageController extends AbstractController
{
    /**
     *
     * imposes a survey at the first connection
     *
     * @Route("/home/page", name="home_page")
     * @param SessionInterface $session
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index(SessionInterface $session, Request $request): Response
    {
        $user = $this->getUser();

        if (!$session->has('isFirstConnection')) {
            $session->set('isFirstConnection', $user->getIsFirstConnection());
        }

        $type = UserInfosComplementaryType::class;
        $form = $this->createForm($type, $user->getInfos());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $data->setCity(ucfirst(strtolower($data->getCity())));
            $user->setIsfirstConnection(true);
            $user->getRating()->setDomProfileType($data->getProfileDominance());
            $entityManager = $this->getDoctrine()->getManager();

            if ($session->get('isFirstConnection') === false) {
                $user->getRating()->setDomProfileType($user->getInfos()->getProfileDominance());
                $session->set('isFirstConnection', true);
                $entityManager->persist($data);
                $entityManager->persist($user);

                $entityManager->flush();
            }

            $this->redirectToRoute('home_page');
        }
        return $this->render('home/home_page/home_page.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'isFirstConnection' => $user->getIsfirstCOnnection(),
        ]);
    }
}
