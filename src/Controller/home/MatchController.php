<?php

namespace App\Controller\home;


use App\Entity\UserLink;
use App\Form\ResearchFilterType;
use App\Repository\UserRepository;
use App\services\DateLibrary;
use App\services\FormatLibrary;
use App\services\GeolocLibrary;
use App\services\LikeLibrary;
use App\services\StatLibrary;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/home")
 */
class MatchController extends AbstractController
{
    /**
     * @Route("/match/", name="match")
     * @param UserRepository $userRepository
     * @param GeolocLibrary $geolocLibrary
     * @param Request $request
     * @param DateLibrary $dateLibrary
     * @param LikeLibrary $likeLibrary
     * @return Response
     * @throws \Exception
     */
    public function index(
        UserRepository $userRepository,
        GeolocLibrary $geolocLibrary,
        Request $request,
        DateLibrary $dateLibrary,
        LikeLibrary $likeLibrary
    ): Response
    {
        $user = $this->getUser();
        $sexType = $user->getSex();

        $currentDislikeGived = explode(',', $user->getMatching()->getDislikeSend());
        $currentLikeSend = explode(',', $user->getMatching()->getLikeSend());
        $currentHaveMatched = explode(',', $user->getMatching()->getHaveMatched());
        $lat1 = $user->getLat();
        $lng1 = $user->getLng();
        $hash = md5($user->getPseudo());
        $stopLike = $likeLibrary->likeTimer($user);

        $userArray = [];
        foreach (
            $userRepository->displayUserForMatch($user,
                $sexType,
                $user->getInfos()->getHereFor(),
                $currentDislikeGived,
                $currentLikeSend,
                $currentHaveMatched
            ) as $item) {
            $lat2 = $item->getLat();
            $lng2 = $item->getLng();
            if ($user->getFilter()->getRangeAgeMin() <= $dateLibrary->convertDobToAge($item) &&
                $dateLibrary->convertDobToAge($item) <= $user->getFilter()->getRangeAgeMax() &&
                $user->getFilter()->getRangeMaxDistance() >= $geolocLibrary->distance($lat1, $lng1, $lat2, $lng2)
            ) {
                $item->distance = $geolocLibrary->distance($lat1, $lng1, $lat2, $lng2);
                array_push($userArray, $item);
                if (count($userArray) >= 1) {
                    break;
                }
            }
        }

        $form = $this->createForm(ResearchFilterType::class, $user->getFilter());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $rangeAgeMin = $data->getRangeAgeMin();
            $rangeAgeMax = $data->getRangeAgeMax();
            $rangeMaxDistance = $data->getRangeMaxDistance();

            if ($rangeAgeMin > $rangeAgeMax) {
                $this->addFlash('rangeError', 'min > max');
                return $this->redirectToRoute("match");
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();

            $interest = $data->getInterest();
            $lifestyle = $data->getLifestyle();
            $characters = $data->getCharacters();

            $userArray = [];
            foreach (
                $userRepository->displayUserForMatch(
                    $user,
                    $sexType,
                    $user->getInfos()->getHereFor(),
                    $currentDislikeGived,
                    $currentLikeSend,
                    $currentHaveMatched,
                    $interest,
                    $lifestyle,
                    $characters
                ) as $item) {
                $lat2 = $item->getLat();
                $lng2 = $item->getLng();
                if ($rangeAgeMin <= $dateLibrary->convertDobToAge($item) &&
                    $dateLibrary->convertDobToAge($item) <= $rangeAgeMax &&
                    $rangeMaxDistance >= $geolocLibrary->distance($lat1, $lng1, $lat2, $lng2)
                ) {
                    $item->distance = $geolocLibrary->distance($lat1, $lng1, $lat2, $lng2);
                    array_push($userArray, $item);
                    if (count($userArray) >= 1) {
                        return $this->render('home/match/match.html.twig', [
                            'sexType' => $sexType,
                            'form' => $form->createView(),
                            'userArray' => $userArray,
                            'hash' => $hash,
                            'stopLike' => $stopLike
                        ]);
                    }
                }
            }
        }
        return $this->render('home/match/match.html.twig', [
            'sexType' => $sexType,
            'form' => $form->createView(),
            'userArray' => $userArray,
            'hash' => $hash,
            'stopLike' => $stopLike
        ]);
    }

    /**
     * @Route("/match/matchingHandlerLike/{id}", name="matching_handler_like")
     * @param string $id
     * @param UserRepository $userRepository
     * @param StatLibrary $statLibrary
     * @param FormatLibrary $formatLibrary
     * @return RedirectResponse
     * @throws \Exception
     */
    public function matchingHandlerLike(string $id, UserRepository $userRepository, StatLibrary $statLibrary, FormatLibrary $formatLibrary): RedirectResponse
    {

        $distantUser = $userRepository->findOneBy(['id' => $id]);
        $user = $this->getUser();

        $likeLimitCount = $user->getMatching()->getLikeLimit();
        $user->getMatching()->setLikeLimit($likeLimitCount + 1);

        $user->getMatching()->setLastLikeAt(new \DateTime());

        // si match
        if (in_array($id, explode(',', $user->getMatching()->getLikeReceived()))) {

            $currenHavedMatched = $formatLibrary->addValueToString($user->getMatching()->getHaveMatched(), $id);
            $user->getMatching()->setHaveMatched($currenHavedMatched);

            $distantHaveMatched = $formatLibrary->addValueToString($distantUser->getMatching()->getHaveMatched(), $user->getId());
            $distantUser->getMatching()->setHaveMatched($distantHaveMatched);

            $statLibrary->hydrateProcessAddMatch($user, $distantUser);

            $newUserLink = new UserLink();
            $newUserLink->setIdCurrent($user->getId());
            $newUserLink->setIdDistant($distantUser->getId());
            $newUserLink->setLink(md5(rand(1, 100000)));
            $newUserLink->setTimer(new \DateTime());

            $this->addFlash('newMatch', 'Vous avez un nouveau match ');

        } else {
            $currentLikeSend = $formatLibrary->addValueToString($user->getMatching()->getLikeSend(), $id);
            $user->getMatching()->setLikeSend($currentLikeSend);

            $distantLikeReceived = $formatLibrary->addValueToString($distantUser->getMatching()->getLikeReceived(), $user->getId());
            $distantUser->getMatching()->setLikeReceived($distantLikeReceived);
        }

        $statLibrary->hydrateProcessAddLike($user, $distantUser);

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($newUserLink)) {
            $entityManager->persist($newUserLink);
        }
        $entityManager->persist($user);
        $entityManager->persist($distantUser);
        $entityManager->flush();

        return $this->redirectToRoute("match");
    }

    /**
     * @Route("/match/matchingHandlerDislike/{id}", name="matching_handler_dislike")
     * @param string $id
     * @param StatLibrary $statLibrary
     * @param UserRepository $userRepository
     * @param FormatLibrary $formatLibrary
     * @return RedirectResponse
     */
    public function matchingHandlerDislike(string $id, StatLibrary $statLibrary, UserRepository $userRepository, FormatLibrary $formatLibrary): RedirectResponse
    {
        $user = $this->getUser();
        $currentDislikeGived = $user->getMatching()->getDislikeSend();
        $tmp = $formatLibrary->addValueToString($currentDislikeGived, $id);
        $user->getMatching()->setDislikeSend($tmp);

        $statLibrary->hydrateDislikeGived($user, $userRepository->findOneBy(['id' => $id]));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute("match");
    }


    /**
     * @Route("/match/matchDisplay", name="match_display")
     * @param UserRepository $userRepository
     * @param GeolocLibrary $geolocLibrary
     * @return Response
     */
    public function matchDisplay(UserRepository $userRepository, GeolocLibrary $geolocLibrary): Response
    {
        $user = $this->getUser();
        $lat1 = $user->getLat();
        $lng1 = $user->getLng();
        $hash = md5($user->getPseudo());
        $haveMatchedArray = explode(',', $user->getMatching()->getHaveMatched());
        $deletedMatch = explode(',', $user->getMatching()->getDeletedMatch());

        $userArray = [];
        foreach ($haveMatchedArray as $item) {
            if (!empty($userRepository->findOneBy(['id' => $item]))) {
                if (!in_array($item, $deletedMatch)) {
                    $distantUser = $userRepository->findOneBy(['id' => $item]);
                    $lat2 = $distantUser->getLat();
                    $lng2 = $distantUser->getLng();
                    $distantUser->distance = $geolocLibrary->distance($lat1, $lng1, $lat2, $lng2);
                    array_push($userArray, $distantUser);
                }
            }
        }
        return $this->render('home/match/matchDisplay.html.twig', [
            'userArray' => $userArray,
            'hash' => $hash
        ]);
    }

    /**
     * @Route("/match/delete_match/{id}", name="delete_match")
     * @param int $id
     * @param UserRepository $userRepository
     * @param StatLibrary $statLibrary
     * @param FormatLibrary $formatLibrary
     * @return RedirectResponse
     */
    public function deleteMatch(int $id, UserRepository $userRepository, StatLibrary $statLibrary, FormatLibrary $formatLibrary): RedirectResponse
    {
        $currentUser = $this->getUser();
        $distantUser = $userRepository->findOneBy(['id' => $id]);

        $currentDeletedMatchUpdtated = $formatLibrary->addValueToString($currentUser->getMatching()->getDeletedMatch(), $id);
        $currentUser->getMatching()->setDeletedMatch($currentDeletedMatchUpdtated);

        $distantDeletedMatchUpdated = $formatLibrary->addValueToString($distantUser->getMatching()->getDeletedMatch(), $currentUser->getId());
        $distantUser->getMatching()->setDeletedMatch($distantDeletedMatchUpdated);

        $currentDeletedMatchWatcherUpdated = $formatLibrary->addValueToString($currentUser->getMatching()->getDeletedMatchWatcher(), $id);
        $currentUser->getMatching()->setDeletedMatchWatcher($currentDeletedMatchWatcherUpdated);

        $statLibrary->hydrateProcessDeleted($currentUser, $distantUser);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentUser);
        $entityManager->persist($distantUser);
        $entityManager->flush();

        return $this->redirectToRoute('match_display');
    }
}