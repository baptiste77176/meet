<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * @Route("/ajax/geoloc", name="ajax_geoloc")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function addGeolocToBdd(Request $request, UserRepository $userRepository): JsonResponse
    {
        $userRepository->setLatAndLng($request->get('lat'), $request->get('lng'), $this->getUser());
        return new JsonResponse("success");
    }
}

