<?php

namespace App\Controller;

use App\Entity\Filter;
use App\Entity\Matching;
use App\Entity\Rating;
use App\Entity\Slider;
use App\Entity\User;
use App\Entity\UserInfos;
use App\Form\RegistrationFormType;
use App\services\DirectoryLibrary;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ValidatorInterface $validator
     * @param DirectoryLibrary $directoryLibrary
     * @return Response
     * @throws \Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator, DirectoryLibrary $directoryLibrary): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $directoryLibrary->createDirectoryForNewUser($user);
            $user->setRoles($user->getRoles());

            $userInfosEntity = new UserInfos();

            $user->setInfos($userInfosEntity);

            $ratingEntity =  new Rating();
            $user->setRating($ratingEntity);

            $sliderEntity = new Slider();
            $user->setSlider($sliderEntity);

            $matchingEntity = new Matching();
            $user->setMatching($matchingEntity);

            $filterEntity = new Filter();
            $filterEntity->setRangeAgeMin(18);
            $filterEntity->setRangeAgeMax(77);
            $filterEntity->setRangeMaxDistance(30);
            $user->setFilter($filterEntity);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash(
                'accountCreated',
                'Your account is created'
            );
            return $this->redirectToRoute('default');
        }
        $errors = $validator->validate($user);

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'errors' => $errors
        ]);
    }
}
