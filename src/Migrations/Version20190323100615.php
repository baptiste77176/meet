<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190323100615 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_infos CHANGE herefor herefor VARCHAR(255) NOT NULL, CHANGE intention intention VARCHAR(255) NOT NULL, CHANGE profile_dominance profile_dominance VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_infos CHANGE herefor herefor VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE intention intention VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE profile_dominance profile_dominance VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
