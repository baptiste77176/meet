<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190320074153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_infos (id INT AUTO_INCREMENT NOT NULL, city VARCHAR(255) DEFAULT NULL, country VARCHAR(255) NOT NULL, sport VARCHAR(255) DEFAULT NULL, work VARCHAR(255) DEFAULT NULL, leisure VARCHAR(255) DEFAULT NULL, interest VARCHAR(255) DEFAULT NULL, lifestyle VARCHAR(255) DEFAULT NULL, characters VARCHAR(255) DEFAULT NULL, origin VARCHAR(255) DEFAULT NULL, eyes VARCHAR(255) DEFAULT NULL, haircolor VARCHAR(255) DEFAULT NULL, haircut VARCHAR(255) DEFAULT NULL, tattoo VARCHAR(255) DEFAULT NULL, piercing VARCHAR(255) DEFAULT NULL, size DOUBLE PRECISION DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, morphology VARCHAR(255) DEFAULT NULL, alcool VARCHAR(255) DEFAULT NULL, smoke VARCHAR(255) DEFAULT NULL, aboutyou LONGTEXT DEFAULT NULL, herefor VARCHAR(255) DEFAULT NULL, intention VARCHAR(255) NOT NULL, children TINYINT(1) DEFAULT NULL, favorite_moovie VARCHAR(255) DEFAULT NULL, favorite_song VARCHAR(255) DEFAULT NULL, profile_dominance VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, sex VARCHAR(255) NOT NULL, dob DATETIME NOT NULL, profilepicture VARCHAR(255) DEFAULT NULL, is_connected TINYINT(1) NOT NULL, last_login_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slider (id INT AUTO_INCREMENT NOT NULL, img LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_infos');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE slider');
    }
}
