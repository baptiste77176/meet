<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190412083557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE filter (id INT AUTO_INCREMENT NOT NULL, herefor VARCHAR(255) NOT NULL, range_age_min INT DEFAULT NULL, range_age_max INT DEFAULT NULL, range_max_distance INT NOT NULL, interest VARCHAR(255) DEFAULT NULL, lifestyle VARCHAR(255) DEFAULT NULL, characters VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD filter_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649D395B25E FOREIGN KEY (filter_id) REFERENCES filter (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649D395B25E ON user (filter_id)');
        $this->addSql('ALTER TABLE user_infos DROP range_age_min, DROP range_age_max, DROP range_max_distance');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649D395B25E');
        $this->addSql('DROP TABLE filter');
        $this->addSql('DROP INDEX UNIQ_8D93D649D395B25E ON user');
        $this->addSql('ALTER TABLE user DROP filter_id');
        $this->addSql('ALTER TABLE user_infos ADD range_age_min INT DEFAULT NULL, ADD range_age_max INT DEFAULT NULL, ADD range_max_distance INT NOT NULL');
    }
}
