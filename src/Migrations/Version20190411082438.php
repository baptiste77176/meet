<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190411082438 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE matching ADD dislike_gived LONGTEXT DEFAULT NULL, DROP dislike_received, CHANGE like_send like_send LONGTEXT DEFAULT NULL, CHANGE like_received like_received LONGTEXT DEFAULT NULL, CHANGE have_matched have_matched LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE matching ADD dislike_received VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP dislike_gived, CHANGE like_send like_send VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE like_received like_received VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE have_matched have_matched VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
