<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /*
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true)
         *
         */

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Email value should not be blank")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Pseudo value should not be blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your pseudo must be at least {{ limit }} characters long",
     *      maxMessage = "Your pseudo cannot be longer than {{ limit }} characters")
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Firstname value should not be blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Lastname value should not be blank")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Sex value should not be blank")
     */
    private $sex;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Date of birth value should not be blank")
     */
    private $dob;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $profilepicture;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConnected;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isFirstConnection;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lng;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserInfos", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Infos;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Slider", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Slider;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPremium;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Rating", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Rating;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Matching", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $matching;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Filter", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Filter;


    /**
     * User constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->dob = new \DateTime();
        $this->isConnected = FALSE;
        $this->isFirstConnection = FALSE;
        $this->isPremium = FALSE;
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string|null
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     * @return User
     */
    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return User
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getSex(): ?string
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     * @return User
     */
    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDob(): \DateTimeInterface
    {
        return $this->dob;
    }

    /**
     * @param \DateTimeInterface $dob
     * @return User
     */
    public function setDob(\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProfilepicture(): ?string
    {
        return $this->profilepicture;
    }

    /**
     * @param string $profilepicture
     * @return User
     */
    public function setProfilepicture(?string $profilepicture): self
    {
        $this->profilepicture = $profilepicture;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsConnected(): ?bool
    {
        return $this->isConnected;
    }

    /**
     * @param bool $isConnected
     * @return User
     */
    public function setIsConnected(bool $isConnected): self
    {
        $this->isConnected = $isConnected;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->lastLoginAt;
    }

    /**
     * @param \DateTimeInterface $lastLoginAt
     * @return User
     */
    public function setLastLoginAt(\DateTimeInterface $lastLoginAt): self
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsFirstConnection(): ?bool
    {
        return $this->isFirstConnection;
    }

    /**
     * @param bool $isFirstConnection
     * @return User
     */
    public function setIsFirstConnection(bool $isFirstConnection): self
    {
        $this->isFirstConnection = $isFirstConnection;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLat(): ?string
    {
        return $this->lat;
    }

    /**
     * @param string|null $lat
     * @return User
     */
    public function setLat(?string $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLng(): ?string
    {
        return $this->lng;
    }

    /**
     * @param string|null $lng
     * @return User
     */
    public function setLng(?string $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return UserInfos|null
     */
    public function getInfos(): ?UserInfos
    {
        return $this->Infos;
    }

    /**
     * @param UserInfos $Infos
     * @return User
     */
    public function setInfos(UserInfos $Infos): self
    {
        $this->Infos = $Infos;

        return $this;
    }

    /**
     * @return Slider|null
     */
    public function getSlider(): ?Slider
    {
        return $this->Slider;
    }

    /**
     * @param Slider $Slider
     * @return User
     */
    public function setSlider(Slider $Slider): self
    {
        $this->Slider = $Slider;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsPremium(): ?bool
    {
        return $this->isPremium;
    }

    /**
     * @param bool $isPremium
     * @return User
     */
    public function setIsPremium(bool $isPremium): self
    {
        $this->isPremium = $isPremium;

        return $this;
    }

    /**
     * @return Rating|null
     */
    public function getRating(): ?Rating
    {
        return $this->Rating;
    }

    /**
     * @param Rating $Rating
     * @return User
     */
    public function setRating(Rating $Rating): self
    {
        $this->Rating = $Rating;

        return $this;
    }

    /**
     * @return Matching|null
     */
    public function getMatching(): ?Matching
    {
        return $this->matching;
    }

    /**
     * @param Matching $matching
     * @return User
     */
    public function setMatching(Matching $matching): self
    {
        $this->matching = $matching;

        return $this;
    }

    /**
     * @return Filter|null
     */
    public function getFilter(): ?Filter
    {
        return $this->Filter;
    }

    /**
     * @param Filter $Filter
     * @return User
     */
    public function setFilter(Filter $Filter): self
    {
        $this->Filter = $Filter;

        return $this;
    }
}
