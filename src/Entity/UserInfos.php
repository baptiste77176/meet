<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserInfosRepository")
 */
class UserInfos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your city name must be at least {{ limit }} characters long",
     *      maxMessage = "Your city name cannot be longer than {{ limit }} characters")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Country
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 5,
     *      max = 30,
     *      minMessage = "Your sport name must be at least {{ limit }} characters long",
     *      maxMessage = "Your sport name cannot be longer than {{ limit }} characters")
     */
    private $sport;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 5,
     *      max = 30,
     *      minMessage = "Your work name must be at least {{ limit }} characters long",
     *      maxMessage = "Your work name cannot be longer than {{ limit }} characters")
     */
    private $work;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = 5,
     *      max = 30,
     *      minMessage = "Your leisure name must be at least {{ limit }} characters long",
     *      maxMessage = "Your leisure name cannot be longer than {{ limit }} characters")
     */
    private $leisure;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $interest;

    const INTEREST_CINEMA = 'cinéma';
    const INTEREST_MUSIC = 'musique';
    const INTEREST_ART = 'art';
    const INTEREST_COOKING = 'cuisine';
    const INTEREST_SPORT = 'sport';
    const INTEREST_LITTERATURE = 'littérature';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lifestyle;

    const LIFESTYLE_GEEK = 'geek';
    const LIFESTYLE_ROOT = 'root';
    const LIFESTYLE_CLASSIC = 'classic';
    const LIFESTYLE_NERD = 'intello';
    const LIFESTYLE_SPORT = 'sport';
    const LIFESTYLE_ENTERPRISING = 'enterprising';
    const LIFESTYLE_TRAVELLER = 'traveller';
    const LIFESTYLE_WORKER = 'worker ';
    const LIFESTYLE_AMBITIOUS = 'ambitious';
    const LIFESTYLE_HOMEBODY = 'homebody';
    const LIFESTYLE_DREAMER = 'dreamer';
    const LIFESTYLE_PARTY = 'party';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $characters;

    const CHARACTERS_ROMANTIC = 'romantic';
    const CHARACTERS_SOCIAL = 'social';
    const CHARACTERS_QUIET = 'quiet';
    const CHARACTERS_DYNAMIC = 'dynamic';
    const CHARACTERS_ECCENTRIC = 'eccentric';
    const CHARACTERS_ATTENTIVE = 'attentive';
    const CHARACTERS_OUTGOING = 'outgoing';
    const CHARACTERS_INDEPENDANT = 'independant';
    const CHARACTERS_DISCREET = 'discreet';
    const CHARACTERS_MALICIOUS = 'malicious';
    const CHARACTERS_CURIOUS = 'curious';


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $origin;

    const ORIGIN_EUROPEAN = 'européennes';
    const ORIGIN_AFRO = 'Afro';
    const ORIGIN_MAGHREB = 'Maghrébines';
    const ORIGIN_ASIA = 'Asiatiques';
    const ORIGIN_METISS = 'Métisses';
    const ORIGIN_EURASIA = 'Eurasiennes';
    const ORIGIN_LATINA = 'Latines';
    const ORIGIN_ANTILLEAN = 'Antillaises';


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eyes;

    const EYES_BLUE = 'Bleu';
    const EYES_BLACK = 'Noir';
    const EYES_BROWN = 'Marron';
    const EYES_PEANUTS = 'Noisette';
    const EYES_GREEN = 'Vert';


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $haircolor;

    const HAIRCOLOR_BLACK = 'black';
    const HAIRCOLOR_DARK_BLOND = 'dark_blond';
    const HAIRCOLOR_RED = 'red';
    const HAIRCOLOR_OTHER = 'other';
    const HAIRCOLOR_BROWN = 'brown';
    const HAIRCOLOR_BLOND = 'blond';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $haircut;

    const HAIRCUT_BALD = 'Chauve';
    const HAIRCUT_SHAVEN = 'Rasé';
    const HAIRCUT_SHORT = 'Court';
    const HAIRCUT_HALF_LONG = 'Mi-long';
    const HAIRCUT_LONG = 'Long';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tattoo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $piercing;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $size;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $morphology;

    const MORPHOLOGY_SLENDER = 'Svelte';
    const MORPHOLOGY_SPORT = 'Sportive';
    const MORPHOLOGY_BALANCED = 'équilibrée';
    const MORPHOLOGY_LUSCIOUS = 'Pulpeuse';
    const MORPHOLOGY_GENEROUS = 'Généreuse';
    const MORPHOLOGY_MUSCLES = 'Muscler';
    const MORPHOLOGY_NORMAL = 'Normal';
    const MORPHOLOGY_DELICATE = 'Fine';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alcool;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smoke;
//, nullable=true
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $aboutyou;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $herefor;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $intention;

    const INTENTION_CDI = 'cdi';
    const INTENTION_CDD = 'cdd';
    const INTENTION_BOTH = 'both';

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $children;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $favoriteMoovie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $favoriteSong;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $profileDominance;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="Infos", cascade={"persist", "remove"})
     */
    private $user;

    const PROFILE_DOM_RECKLESS = 'reckless';
    const PROFILE_DOM_SHY = 'shy';


    const YES = '1';
    const NO = '0';

    const FREQUENCY_NEVER = 'Jamais';
    const FREQUENCY_RARE = 'Rarement';
    const FREQUENCY_OCCASIONALLY = 'Occasionnellement';
    const FREQUENCY_OFTEN = 'Souvent';

    const SEX_TYPE_MEN = 'homme';
    const SEX_TYPE_WOMEN = 'femme';

    /**
     * UserInfos constructor.
     */
    public function __construct()
    {
        $this->city = "";
        $this->intention = "not set";
        $this->profileDominance = "not set";
        $this->herefor = "not set";
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return UserInfos
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return UserInfos
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSport(): ?string
    {
        return $this->sport;
    }

    /**
     * @param string|null $sport
     * @return UserInfos
     */
    public function setSport(?string $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWork(): ?string
    {
        return $this->work;
    }

    /**
     * @param string|null $work
     * @return UserInfos
     */
    public function setWork(?string $work): self
    {
        $this->work = $work;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLeisure(): ?string
    {
        return $this->leisure;
    }

    /**
     * @param string|null $leisure
     * @return UserInfos
     */
    public function setLeisure(?string $leisure): self
    {
        $this->leisure = $leisure;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInterest(): ?string
    {
        return $this->interest;
    }

    /**
     * @param string|null $interest
     * @return UserInfos
     */
    public function setInterest(?string $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLifestyle(): ?string
    {
        return $this->lifestyle;
    }

    /**
     * @param string|null $lifestyle
     * @return UserInfos
     */
    public function setLifestyle(?string $lifestyle): self
    {
        $this->lifestyle = $lifestyle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCharacters(): ?string
    {
        return $this->characters;
    }

    /**
     * @param string|null $characters
     * @return UserInfos
     */
    public function setCharacters(?string $characters): self
    {
        $this->characters = $characters;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @param string|null $origin
     * @return UserInfos
     */
    public function setOrigin(?string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEyes(): ?string
    {
        return $this->eyes;
    }

    /**
     * @param string|null $eyes
     * @return UserInfos
     */
    public function setEyes(?string $eyes): self
    {
        $this->eyes = $eyes;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHaircolor(): ?string
    {
        return $this->haircolor;
    }

    /**
     * @param string|null $haircolor
     * @return UserInfos
     */
    public function setHaircolor(?string $haircolor): self
    {
        $this->haircolor = $haircolor;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHaircut(): ?string
    {
        return $this->haircut;
    }

    /**
     * @param string|null $haircut
     * @return UserInfos
     */
    public function setHaircut(?string $haircut): self
    {
        $this->haircut = $haircut;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTattoo(): ?string
    {
        return $this->tattoo;
    }

    /**
     * @param string|null $tattoo
     * @return UserInfos
     */
    public function setTattoo(?string $tattoo): self
    {
        $this->tattoo = $tattoo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPiercing(): ?string
    {
        return $this->piercing;
    }

    /**
     * @param string|null $piercing
     * @return UserInfos
     */
    public function setPiercing(?string $piercing): self
    {
        $this->piercing = $piercing;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSize(): ?float
    {
        return $this->size;
    }

    /**
     * @param float|null $size
     * @return UserInfos
     */
    public function setSize(?float $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     * @return UserInfos
     */
    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMorphology(): ?string
    {
        return $this->morphology;
    }

    /**
     * @param string|null $morphology
     * @return UserInfos
     */
    public function setMorphology(?string $morphology): self
    {
        $this->morphology = $morphology;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlcool(): ?string
    {
        return $this->alcool;
    }

    /**
     * @param string|null $alcool
     * @return UserInfos
     */
    public function setAlcool(?string $alcool): self
    {
        $this->alcool = $alcool;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSmoke(): ?string
    {
        return $this->smoke;
    }

    /**
     * @param string|null $smoke
     * @return UserInfos
     */
    public function setSmoke(?string $smoke): self
    {
        $this->smoke = $smoke;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAboutyou(): ?string
    {
        return $this->aboutyou;
    }

    /**
     * @param string|null $aboutyou
     * @return UserInfos
     */
    public function setAboutyou(?string $aboutyou): self
    {
        $this->aboutyou = $aboutyou;

        return $this;
    }

    /**
     * @return string
     */
    public function getHerefor(): ?string
    {
        return $this->herefor;
    }

    /**
     * @param string $herefor
     * @return UserInfos
     */
    public function setHerefor(?string $herefor): self
    {
        $this->herefor = $herefor;

        return $this;
    }

    /**
     * @return string
     */
    public function getIntention(): ?string
    {
        return $this->intention;
    }

    /**
     * @param string $intention
     * @return UserInfos
     */
    public function setIntention(?string $intention): self
    {
        $this->intention = $intention;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getChildren(): ?bool
    {
        return $this->children;
    }

    /**
     * @param bool|null $children
     * @return UserInfos
     */
    public function setChildren(?bool $children): self
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFavoriteMoovie(): ?string
    {
        return $this->favoriteMoovie;
    }

    /**
     * @param string|null $favoriteMoovie
     * @return UserInfos
     */
    public function setFavoriteMoovie(?string $favoriteMoovie): self
    {
        $this->favoriteMoovie = $favoriteMoovie;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFavoriteSong(): ?string
    {
        return $this->favoriteSong;
    }

    /**
     * @param string|null $favoriteSong
     * @return UserInfos
     */
    public function setFavoriteSong(?string $favoriteSong): self
    {
        $this->favoriteSong = $favoriteSong;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileDominance(): ?string
    {
        return $this->profileDominance;
    }

    /**
     * @param string $profileDominance
     * @return UserInfos
     */
    public function setProfileDominance(?string $profileDominance): self
    {
        $this->profileDominance = $profileDominance;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserInfos
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        // set the owning side of the relation if necessary
        if ($this !== $user->getInfos()) {
            $user->setInfos($this);
        }

        return $this;
    }
}
