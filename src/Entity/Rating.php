<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RatingRepository")
 */
class Rating
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $likeReceived;

    /**
     * @ORM\Column(type="integer")
     */
    private $dislikeSend;

    /**
     * @ORM\Column(type="integer")
     */
    private $likeSend;

    /**
     * @ORM\Column(type="integer")
     */
    private $matchCounter;

    /**
     * @ORM\Column(type="integer")
     */
    private $connectionFrequencyByYears;

    /**
     * @ORM\Column(type="integer")
     */
    private $connectionFrequencyByMonth;

    /**
     * @ORM\Column(type="integer")
     */
    private $connectionFrequencyTotal;


    /**
     * @ORM\Column(type="integer")
     */
    private $totalProfileVisited;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalProfileVisitor;

    /**
     * @ORM\Column(type="integer")
     */
    private $profilePictureChange;

    /**
     * @ORM\Column(type="integer")
     */
    private $sliderAdd;

    /**
     * @ORM\Column(type="integer")
     */
    private $messageReactivity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reputationWatcher;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $domProfileType;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="Rating", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $dislikeReceived;

    /**
     * @ORM\Column(type="integer")
     */
    private $haveDelete;

    /**
     * @ORM\Column(type="integer")
     */
    private $haveBeenDeleted;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $profileVisited;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $profileVisitor;


    /**
     * Rating constructor.
     */
    public function __construct()
    {
        $this->connectionFrequencyTotal = 0;
        $this->connectionFrequencyByMonth = 0;
        $this->connectionFrequencyByYears = 0;
        $this->likeReceived = 0;
        $this->dislikeSend = 0;
        $this->dislikeReceived = 0;
        $this->likeSend = 0;
        $this->matchCounter = 0;
        $this->totalProfileVisited = 0;
        $this->totalProfileVisitor = 0;
        $this->profilePictureChange = 0;
        $this->sliderAdd = 0;
        $this->messageReactivity = 0;
        $this->haveDelete = 0;
        $this->haveBeenDeleted = 0;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getLikeReceived(): ?int
    {
        return $this->likeReceived;
    }

    /**
     * @param int $likeReceived
     * @return Rating
     */
    public function setLikeReceived(int $likeReceived): self
    {
        $this->likeReceived = $likeReceived;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDislikeSend(): ?int
    {
        return $this->dislikeSend;
    }

    /**
     * @param int $dislikeSend
     * @return Rating
     */
    public function setDislikeSend(int $dislikeSend): self
    {
        $this->dislikeSend = $dislikeSend;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLikeSend(): ?int
    {
        return $this->likeSend;
    }

    /**
     * @param int $likeSend
     * @return Rating
     */
    public function setLikeSend(int $likeSend): self
    {
        $this->likeSend = $likeSend;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMatchCounter(): ?int
    {
        return $this->matchCounter;
    }

    /**
     * @param int $matchCounter
     * @return Rating
     */
    public function setMatchCounter(int $matchCounter): self
    {
        $this->matchCounter = $matchCounter;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getConnectionFrequency(): ?int
    {
        return $this->connectionFrequencyByYears;
    }

    /**
     * @param int $connectionFrequencyByYears
     * @return Rating
     */
    public function setConnectionFrequency(int $connectionFrequencyByYears): self
    {
        $this->connectionFrequencyByYears = $connectionFrequencyByYears;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReputationWatcher(): ?string
    {
        return $this->reputationWatcher;
    }

    /**
     * @param string|null $reputationWatcher
     * @return Rating
     */
    public function setReputationWatcher(?string $reputationWatcher): self
    {
        $this->reputationWatcher = $reputationWatcher;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDomProfileType(): ?string
    {
        return $this->domProfileType;
    }

    /**
     * @param string $domProfileType
     * @return Rating
     */
    public function setDomProfileType(string $domProfileType): self
    {
        $this->domProfileType = $domProfileType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTotalProfileVisited(): ?int
    {
        return $this->totalProfileVisited;
    }

    /**
     * @param int $totalProfileVisited
     * @return Rating
     */
    public function setTotalProfileVisited(int $totalProfileVisited): self
    {
        $this->totalProfileVisited = $totalProfileVisited;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getProfilePictureChange(): ?int
    {
        return $this->profilePictureChange;
    }

    /**
     * @param int $profilePictureChange
     * @return Rating
     */
    public function setProfilePictureChange(int $profilePictureChange): self
    {
        $this->profilePictureChange = $profilePictureChange;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMessgeReactivity(): ?int
    {
        return $this->messageReactivity;
    }

    /**
     * @param int $messageReactivity
     * @return Rating
     */
    public function setMessgeReactivity(int $messageReactivity): self
    {
        $this->messageReactivity = $messageReactivity;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getConnectionFrequencyByMonth(): ?int
    {
        return $this->connectionFrequencyByMonth;
    }

    /**
     * @param int $connectionFrequencyByMonth
     * @return Rating
     */
    public function setConnectionFrequencyByMonth(int $connectionFrequencyByMonth): self
    {
        $this->connectionFrequencyByMonth = $connectionFrequencyByMonth;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Rating
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        // set the owning side of the relation if necessary
        if ($this !== $user->getRating()) {
            $user->setRating($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConnectionFrequencyByYears()
    {
        return $this->connectionFrequencyByYears;
    }

    /**
     * @param mixed $connectionFrequencyByYears
     */
    public function setConnectionFrequencyByYears($connectionFrequencyByYears): void
    {
        $this->connectionFrequencyByYears = $connectionFrequencyByYears;
    }

    /**
     * @return mixed
     */
    public function getMessageReactivity()
    {
        return $this->messageReactivity;
    }

    /**
     * @param mixed $messageReactivity
     */
    public function setMessageReactivity($messageReactivity): void
    {
        $this->messageReactivity = $messageReactivity;
    }

    /**
     * @return int|null
     */
    public function getConnectionFrequencyTotal(): ?int
    {
        return $this->connectionFrequencyTotal;
    }

    /**
     * @param int $connectionFrequencyTotal
     * @return Rating
     */
    public function setConnectionFrequencyTotal(int $connectionFrequencyTotal): self
    {
        $this->connectionFrequencyTotal = $connectionFrequencyTotal;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTotalProfileVisitor(): ?int
    {
        return $this->totalProfileVisitor;
    }

    /**
     * @param int $totalProfileVisitor
     * @return Rating
     */
    public function setTotalProfileVisitor(int $totalProfileVisitor): self
    {
        $this->totalProfileVisitor = $totalProfileVisitor;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSliderAdd(): ?int
    {
        return $this->sliderAdd;
    }

    /**
     * @param int $sliderAdd
     * @return Rating
     */
    public function setSliderAdd(int $sliderAdd): self
    {
        $this->sliderAdd = $sliderAdd;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDislikeReceived(): ?int
    {
        return $this->dislikeReceived;
    }

    /**
     * @param int $dislikeReceived
     * @return Rating
     */
    public function setDislikeReceived(int $dislikeReceived): self
    {
        $this->dislikeReceived = $dislikeReceived;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHaveDelete(): ?int
    {
        return $this->haveDelete;
    }

    /**
     * @param int $haveDelete
     * @return Rating
     */
    public function setHaveDelete(int $haveDelete): self
    {
        $this->haveDelete = $haveDelete;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHaveBeenDeleted(): ?int
    {
        return $this->haveBeenDeleted;
    }

    /**
     * @param int $haveBeenDeleted
     * @return Rating
     */
    public function setHaveBeenDeleted(int $haveBeenDeleted): self
    {
        $this->haveBeenDeleted = $haveBeenDeleted;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProfileVisited(): ?string
    {
        return $this->profileVisited;
    }

    /**
     * @param string|null $profileVisited
     * @return Rating
     */
    public function setProfileVisited(?string $profileVisited): self
    {
        $this->profileVisited = $profileVisited;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProfileVisitor(): ?string
    {
        return $this->profileVisitor;
    }

    /**
     * @param string|null $profileVisitor
     * @return Rating
     */
    public function setProfileVisitor(?string $profileVisitor): self
    {
        $this->profileVisitor = $profileVisitor;

        return $this;
    }
}
