<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserLinkRepository")
 */
class UserLink
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idCurrent;

    /**
     * @ORM\Column(type="integer")
     */
    private $idDistant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timer;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getIdCurrent(): ?int
    {
        return $this->idCurrent;
    }

    /**
     * @param int $idCurrent
     * @return UserLink
     */
    public function setIdCurrent(int $idCurrent): self
    {
        $this->idCurrent = $idCurrent;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIdDistant(): ?int
    {
        return $this->idDistant;
    }

    /**
     * @param int $idDistant
     * @return UserLink
     */
    public function setIdDistant(int $idDistant): self
    {
        $this->idDistant = $idDistant;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return UserLink
     */
    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimer(): ?\DateTimeInterface
    {
        return $this->timer;
    }

    /**
     * @param \DateTimeInterface $timer
     * @return UserLink
     */
    public function setTimer(\DateTimeInterface $timer): self
    {
        $this->timer = $timer;

        return $this;
    }
}
