<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterRepository")
 */
class Filter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rangeAgeMin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rangeAgeMax;

    /**
     * @ORM\Column(type="integer")
     */
    private $rangeMaxDistance;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $interest;

    /**
     *
     */
    const INTEREST_CINEMA = 'cinéma';
    const INTEREST_MUSIC = 'musique';
    const INTEREST_ART = 'art';
    const INTEREST_COOKING = 'cuisine';
    const INTEREST_SPORT = 'sport';
    const INTEREST_LITTERATURE = 'littérature';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lifestyle;

    const LIFESTYLE_GEEK = 'geek';
    const LIFESTYLE_ROOT = 'root';
    const LIFESTYLE_CLASSIC = 'classic';
    const LIFESTYLE_NERD = 'intello';
    const LIFESTYLE_SPORT = 'sport';
    const LIFESTYLE_ENTERPRISING = 'enterprising';
    const LIFESTYLE_TRAVELLER = 'traveller';
    const LIFESTYLE_WORKER = 'worker ';
    const LIFESTYLE_AMBITIOUS = 'ambitious';
    const LIFESTYLE_HOMEBODY = 'homebody';
    const LIFESTYLE_DREAMER = 'dreamer';
    const LIFESTYLE_PARTY = 'party';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $characters;

    const CHARACTERS_ROMANTIC = 'romantic';
    const CHARACTERS_SOCIAL = 'social';
    const CHARACTERS_QUIET = 'quiet';
    const CHARACTERS_DYNAMIC = 'dynamic';
    const CHARACTERS_ECCENTRIC = 'eccentric';
    const CHARACTERS_ATTENTIVE = 'attentive';
    const CHARACTERS_OUTGOING = 'outgoing';
    const CHARACTERS_INDEPENDANT = 'independant';
    const CHARACTERS_DISCREET = 'discreet';
    const CHARACTERS_MALICIOUS = 'malicious';
    const CHARACTERS_CURIOUS = 'curious';

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="Filter", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return int|null
     */
    public function getRangeAgeMin(): ?int
    {
        return $this->rangeAgeMin;
    }

    /**
     * @param int|null $rangeAgeMin
     * @return Filter
     */
    public function setRangeAgeMin(?int $rangeAgeMin): self
    {
        $this->rangeAgeMin = $rangeAgeMin;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRangeAgeMax(): ?int
    {
        return $this->rangeAgeMax;
    }

    /**
     * @param int|null $rangeAgeMax
     * @return Filter
     */
    public function setRangeAgeMax(?int $rangeAgeMax): self
    {
        $this->rangeAgeMax = $rangeAgeMax;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRangeMaxDistance(): ?int
    {
        return $this->rangeMaxDistance;
    }

    /**
     * @param int $rangeMaxDistance
     * @return Filter
     */
    public function setRangeMaxDistance(int $rangeMaxDistance): self
    {
        $this->rangeMaxDistance = $rangeMaxDistance;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInterest(): ?string
    {
        return $this->interest;
    }

    /**
     * @param string|null $interest
     * @return Filter
     */
    public function setInterest(?string $interest): self
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLifestyle(): ?string
    {
        return $this->lifestyle;
    }

    /**
     * @param string|null $lifestyle
     * @return Filter
     */
    public function setLifestyle(?string $lifestyle): self
    {
        $this->lifestyle = $lifestyle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCharacters(): ?string
    {
        return $this->characters;
    }

    /**
     * @param string|null $characters
     * @return Filter
     */
    public function setCharacters(?string $characters): self
    {
        $this->characters = $characters;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Filter
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        // set the owning side of the relation if necessary
        if ($this !== $user->getFilter()) {
            $user->setFilter($this);
        }

        return $this;
    }
}
