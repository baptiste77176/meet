<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchingRepository")
 */
class Matching
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $likeSend;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $likeReceived;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $haveMatched;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $dislikeSend;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="matching", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $deletedMatch;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $deletedMatchWatcher;

    /**
     * @ORM\Column(type="integer")
     */
    private $likeLimit;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $likeTimer;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLikeAt;

    /**
     * Matching constructor.
     */
    public function __construct()
    {
        $this->likeLimit = 0;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLikeSend(): ?string
    {
        return $this->likeSend;
    }

    /**
     * @param string|null $likeSend
     * @return Matching
     */
    public function setLikeSend(?string $likeSend): self
    {
        $this->likeSend = $likeSend;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLikeReceived(): ?string
    {
        return $this->likeReceived;
    }

    /**
     * @param string|null $likeReceived
     * @return Matching
     */
    public function setLikeReceived(?string $likeReceived): self
    {
        $this->likeReceived = $likeReceived;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHaveMatched(): ?string
    {
        return $this->haveMatched;
    }

    /**
     * @param string|null $haveMatched
     * @return Matching
     */
    public function setHaveMatched(?string $haveMatched): self
    {
        $this->haveMatched = $haveMatched;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDislikeSend(): ?string
    {
        return $this->dislikeSend;
    }

    /**
     * @param string|null $dislikeSend
     * @return Matching
     */
    public function setDislikeSend(?string $dislikeSend): self
    {
        $this->dislikeSend = $dislikeSend;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Matching
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        // set the owning side of the relation if necessary
        if ($this !== $user->getMatching()) {
            $user->setMatching($this);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeletedMatch(): ?string
    {
        return $this->deletedMatch;
    }

    /**
     * @param string|null $deletedMatch
     * @return Matching
     */
    public function setDeletedMatch(?string $deletedMatch): self
    {
        $this->deletedMatch = $deletedMatch;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeletedMatchWatcher(): ?string
    {
        return $this->deletedMatchWatcher;
    }

    /**
     * @param string|null $deletedMatchWatcher
     * @return Matching
     */
    public function setDeletedMatchWatcher(?string $deletedMatchWatcher): self
    {
        $this->deletedMatchWatcher = $deletedMatchWatcher;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLikeLimit(): ?int
    {
        return $this->likeLimit;
    }

    /**
     * @param int $likeLimit
     * @return Matching
     */
    public function setLikeLimit(int $likeLimit): self
    {
        $this->likeLimit = $likeLimit;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getLikeTimer(): ?\DateTimeInterface
    {
        return $this->likeTimer;
    }

    /**
     * @param \DateTimeInterface|null $likeTimer
     * @return Matching
     */
    public function setLikeTimer(?\DateTimeInterface $likeTimer): self
    {
        $this->likeTimer = $likeTimer;

        return $this;
    }

    public function getLastLikeAt(): ?\DateTimeInterface
    {
        return $this->lastLikeAt;
    }

    public function setLastLikeAt(?\DateTimeInterface $lastLikeAt): self
    {
        $this->lastLikeAt = $lastLikeAt;

        return $this;
    }
}
