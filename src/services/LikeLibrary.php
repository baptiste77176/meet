<?php

namespace App\services;

use App\Entity\User;
use App\Entity\UserInfos;
use Doctrine\ORM\EntityManagerInterface;

class LikeLibrary
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * StatLibrary constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function likeTimer(User $user)
    {
        $dateNextDay = $user->getMatching()->getLikeTimer();
        $actualDate = new \DateTime();
        if (empty($dateNextDay)) {
            if ($user->getMatching()->getLikeLimit() === 50 && $user->getSex() == UserInfos::SEX_TYPE_MEN) {
                $user->getMatching()->setLikeTimer(new \Datetime('now +1 day'));
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                return true;
            }

            if ($user->getMatching()->getLikeLimit() === 80 && $user->getSex() == UserInfos::SEX_TYPE_WOMEN) {
                $user->getMatching()->setLikeTimer(new \Datetime('now +1 day'));
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                return true;
            }
        } else {
            if ($dateNextDay >= $actualDate) {
                $interval = $dateNextDay->diff($actualDate);
                return $interval->format('%h heures et %i minutes');
            } else {
                $user->getMatching()->setLikeTimer(null);
                $user->getMatching()->setLikeLimit(0);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }
        $lastLikeAt = $user->getMatching()->getLastLikeAt();
        if (isset($lastLikeAt)) {
            if ($lastLikeAt->modify('+1 day') <= $actualDate) {
                $user->getMatching()->setLikeTimer(null);
                $user->getMatching()->setLikeLimit(0);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }


        return false;
    }
}