<?php

namespace App\services;

use App\Entity\User;

class DirectoryLibrary
{
    /**
     * @param User $user
     */
    public function createDirectoryForNewUserFixtures(User $user)
    {
        $dir = getcwd() . '/public/img/' . $user->getPseudo();
        $dirProfile = $dir . '/Profile';
        $dirSlider = $dir . '/Slider';
        mkdir($dir);
        mkdir($dirSlider);
        mkdir($dirProfile);
    }

    /**
     * @param User $user
     */
    public function createDirectoryForNewUser(User $user)
    {
        $dir = getcwd() . '/img/' . $user->getPseudo();
        $dirProfile = $dir . '/Profile';
        $dirSlider = $dir . '/Slider';
        mkdir($dir);
        mkdir($dirSlider);
        mkdir($dirProfile);
    }
}