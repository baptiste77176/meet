<?php

namespace App\services;

class GeolocLibrary
{
    /**
     * @param float|null $lat1
     * @param float|null $lng1
     * @param float|null $lat2
     * @param float|null $lng2
     * @return float|int
     */
    public function distance(?float $lat1, ?float $lng1, ?float $lat2, ?float $lng2)
    {
        $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        $rlo1 = deg2rad($lng1);
        $rla1 = deg2rad($lat1);
        $rlo2 = deg2rad($lng2);
        $rla2 = deg2rad($lat2);
        $dlo = ($rlo2 - $rlo1) / 2;
        $dla = ($rla2 - $rla1) / 2;
        $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo));
        $d = 2 * atan2(sqrt($a), sqrt(1 - $a));

        $meter = ($earth_radius * $d);
        $meter = $meter / 1000; // mètres->km

        preg_match('/(\d.*)\./', $meter, $match);
        if (isset($match[1])) {
            $meter = intval($match[1]);
        } else {

            $meter = intval($meter);
        }
        return $meter;
    }
}