<?php

namespace App\services;


use App\Entity\User;

class DateLibrary
{
    /**
     * @param User $user
     * @return string
     * @throws \Exception
     */
    public function convertDobToAge(User $user): string
    {
        $datetime1 = new \DateTime();
        $datetime2 = $user->getDob();
        $age = $datetime1->diff($datetime2);
        return $age->format('%y');
    }
}