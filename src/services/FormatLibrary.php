<?php

namespace App\services;

class FormatLibrary
{
    /**
     * @param string|null $string
     * @param string|null $id
     * @return string|null
     */
    public function addValueToString(?string $string, ?string $id): ?string
    {
        $tmp = explode(',', $string);
        array_push($tmp, $id);
        if ($tmp[0] == "") {
            unset($tmp[0]);
        }
        return implode(',', $tmp);
    }
}