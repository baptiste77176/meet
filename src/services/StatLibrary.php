<?php

namespace App\services;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class StatLibrary
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var FormatLibrary
     */
    protected $formatLibrary;

    /**
     * StatLibrary constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormatLibrary $formatLibrary
     */
    public function __construct(EntityManagerInterface $entityManager, FormatLibrary $formatLibrary)
    {
        $this->entityManager = $entityManager;
        $this->formatLibrary = $formatLibrary;
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function hydrateConnectionFreqency(User $user)
    {
        $lastLogin = $user->getLastLoginAt();

        $countTotal = $user->getRating()->getConnectionFrequencyTotal();
        $countYears = $user->getRating()->getConnectionFrequencyByYears();
        $countMonth = $user->getRating()->getConnectionFrequencyByMonth();

        $today = new \DateTime();

        $actualYears = $today->format('Y');
        $actualMonth = $today->format('m');

        if ($lastLogin !== null) {

            $lastLoginYears = $lastLogin->format('Y');
            $lastLoginMonth = $lastLogin->format('m');

            $user->getRating()->setConnectionFrequencyTotal($countTotal + 1);

            if ($actualYears == $lastLoginYears) {
                $user->getRating()->setConnectionFrequencyByYears($countYears + 1);
            } else {
                $user->getRating()->setConnectionFrequencyByYears(0);
            }

            if ($actualMonth == $lastLoginMonth) {
                $user->getRating()->setConnectionFrequencyByMonth($countMonth + 1);
            } else {
                $user->getRating()->setConnectionFrequencyByMonth(0);
            }
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }

    /**
     * @param User $user
     */
    public function hydrateProfilePictureChange(User $user)
    {
        $count = $user->getRating()->getProfilePictureChange();
        $user->getRating()->setProfilePictureChange($count + 1);
    }

    /**
     * @param User $user
     */
    public function hydrateSLiderAdd(User $user)
    {
        $count = $user->getRating()->getSliderAdd();
        $user->getRating()->setSliderAdd($count + 1);
    }

    /**
     * @param User $current
     * @param User $distant
     */
    public function hydrateProcessAddLike(User $current, User $distant)
    {
        $countCurrent = $current->getRating()->getLikeSend();
        $current->getRating()->setLikeSend($countCurrent + 1);

        $countDistant = $distant->getRating()->getLikeReceived();
        $distant->getRating()->setLikeReceived($countDistant + 1);
    }

    /**
     * @param User $current
     * @param User $distant
     */
    public function hydrateProcessAddMatch(User $current, User $distant)
    {
        $countCurrent = $current->getRating()->getMatchCounter();
        $current->getRating()->setMatchCounter($countCurrent + 1);

        $countDistant = $distant->getRating()->getMatchCounter();
        $distant->getRating()->setMatchCounter($countDistant + 1);
    }

    /**
     * @param User $user
     * @param User $distant
     */
    public function hydrateDislikeGived(User $user, User $distant)
    {
        $countCurrent = $user->getRating()->getDislikeSend();
        $user->getRating()->setDislikeSend($countCurrent + 1);

        $countDistant = $distant->getRating()->getDislikeReceived();
        $distant->getRating()->setDislikeReceived($countDistant + 1);
    }

    /**
     * @param User $current
     * @param User $distant
     */
    public function hydrateProcessDeleted(User $current, User $distant)
    {
        $countCurrent = $current->getRating()->getHaveDelete();
        $current->getRating()->setHaveDelete($countCurrent + 1);

        $countDistant = $distant->getRating()->getHaveBeenDeleted();
        $distant->getRating()->setHaveBeenDeleted($countDistant + 1);
    }

    /**
     * @param User $user
     * @param User $distantUser
     */
    public function hydrateVisiteHandler(User $user, User $distantUser)
    {
        $countCurrent = $user->getRating()->getTotalProfileVisited();
        $user->getRating()->setTotalProfileVisited($countCurrent + 1);

        $countDistant = $distantUser->getRating()->getTotalProfileVisitor();
        $distantUser->getRating()->setTotalProfileVisitor($countDistant + 1);

        $currentNewVisited = $this->formatLibrary->addValueToString($user->getRating()->getProfileVisited(), $distantUser->getId());
        $user->getRating()->setProfileVisited($currentNewVisited);

        $distantNewVisitor = $this->formatLibrary->addValueToString($distantUser->getRating()->getProfileVisitor(), $user->getId());
        $distantUser->getRating()->setProfileVisitor($distantNewVisitor);

        $this->entityManager->persist($user);
        $this->entityManager->persist($distantUser);
        $this->entityManager->flush();
    }
}