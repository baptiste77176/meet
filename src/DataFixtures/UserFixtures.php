<?php

namespace App\DataFixtures;

use App\Entity\Filter;
use App\Entity\Matching;
use App\Entity\Rating;
use App\Entity\Slider;
use App\Entity\User;
use App\Entity\UserInfos;
use App\services\DirectoryLibrary;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class UserFixtures extends Fixture
{
    private $passwordEncoder;
    private $directoryLibrary;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, DirectoryLibrary $directoryLibrary)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->directoryLibrary = $directoryLibrary;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $interest = [
            UserInfos::INTEREST_ART,
            UserInfos::INTEREST_CINEMA,
            UserInfos::INTEREST_COOKING,
            UserInfos::INTEREST_LITTERATURE,
            UserInfos::INTEREST_MUSIC,
            UserInfos::INTEREST_SPORT,
        ];
        $lifeStyle = [
            UserInfos::LIFESTYLE_CLASSIC,
            UserInfos::LIFESTYLE_GEEK,
            UserInfos::LIFESTYLE_NERD,
            UserInfos::LIFESTYLE_ROOT,
            UserInfos::LIFESTYLE_AMBITIOUS,
            UserInfos::LIFESTYLE_DREAMER,
            UserInfos::LIFESTYLE_ENTERPRISING,
            UserInfos::LIFESTYLE_HOMEBODY,
            UserInfos::LIFESTYLE_PARTY,
            UserInfos::LIFESTYLE_SPORT,
            UserInfos::LIFESTYLE_TRAVELLER,
            UserInfos::LIFESTYLE_WORKER,
        ];


        $characters = [
            UserInfos::CHARACTERS_DYNAMIC,
            UserInfos::CHARACTERS_ECCENTRIC,
            UserInfos::CHARACTERS_QUIET,
            UserInfos::CHARACTERS_SOCIAL,
            UserInfos::CHARACTERS_ROMANTIC,
            UserInfos::CHARACTERS_ATTENTIVE,
            UserInfos::CHARACTERS_CURIOUS,
            UserInfos::CHARACTERS_DISCREET,
            UserInfos::CHARACTERS_INDEPENDANT,
            UserInfos::CHARACTERS_MALICIOUS,
            UserInfos::CHARACTERS_OUTGOING,
        ];

        $origin = [
            UserInfos::ORIGIN_AFRO,
            UserInfos::ORIGIN_ANTILLEAN,
            UserInfos::ORIGIN_ASIA,
            UserInfos::ORIGIN_EURASIA,
            UserInfos::ORIGIN_EUROPEAN,
            UserInfos::ORIGIN_MAGHREB,
            UserInfos::ORIGIN_LATINA,
            UserInfos::ORIGIN_METISS,
        ];

        $eyes = [
            UserInfos::EYES_BLACK,
            UserInfos::EYES_BLUE,
            UserInfos::EYES_BROWN,
            UserInfos::EYES_GREEN,
            UserInfos::EYES_PEANUTS,
        ];

        $hairCollor = [
            UserInfos::HAIRCOLOR_BLACK,
            UserInfos::HAIRCOLOR_DARK_BLOND,
            UserInfos::HAIRCOLOR_OTHER,
            UserInfos::HAIRCOLOR_RED,
            UserInfos::HAIRCOLOR_BLOND,
            UserInfos::HAIRCOLOR_BROWN,
        ];

        $hairCut = [
            UserInfos::HAIRCUT_BALD,
            UserInfos::HAIRCUT_HALF_LONG,
            UserInfos::HAIRCUT_LONG,
            UserInfos::HAIRCUT_SHAVEN,
            UserInfos::HAIRCUT_SHORT,
        ];

        $morphhology = [
            UserInfos::MORPHOLOGY_DELICATE,
            UserInfos::MORPHOLOGY_BALANCED,
            UserInfos::MORPHOLOGY_GENEROUS,
            UserInfos::MORPHOLOGY_LUSCIOUS,
            UserInfos::MORPHOLOGY_MUSCLES,
            UserInfos::MORPHOLOGY_NORMAL,
            UserInfos::MORPHOLOGY_SLENDER,
            UserInfos::MORPHOLOGY_SPORT,
        ];

        $intention = [
            UserInfos::INTENTION_BOTH,
            UserInfos::INTENTION_CDD,
            UserInfos::INTENTION_CDI,
        ];

        $profileDom = [
            UserInfos::PROFILE_DOM_RECKLESS,
            UserInfos::PROFILE_DOM_SHY,
        ];

        $hereFor = [
            UserInfos::SEX_TYPE_MEN,
            UserInfos::SEX_TYPE_WOMEN,
        ];

        $frequency = [
            UserInfos::FREQUENCY_NEVER,
            UserInfos::FREQUENCY_OCCASIONALLY,
            UserInfos::FREQUENCY_OFTEN,
            UserInfos::FREQUENCY_RARE,
        ];

        $closeAnswer = [
            UserInfos::YES,
            UserInfos::NO,
            null,

        ];

        $lat = [
            50.8892579,
            48.8892279,
            43.8892579,
            48.8892559,
            47.8898579,
            48.8892599
        ];

        $lng = [
            2.2562355,
            3.3542355,
            1.3862355,
            2.3564355,
            4.3562055,
            2.3162355,
        ];

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $userMale = new User();
            $userMale->setFirstname($faker->firstNameMale);
            $userMale->setLastname($faker->lastName);
            $userMale->setPseudo($faker->firstNameMale);
            $userMale->setSex('homme');
            $userMale->setDob(new \DateTime(rand(1980, 2000) . "-" . rand(1, 12) . "-" . rand(1, 31)));
            $userMale->setEmail($i . $faker->email);
            $userMale->setRoles(['ROLE_USER']);
            $userMale->setPassword($this->passwordEncoder->encodePassword(
                $userMale,
                'debug0000'
            ));
            $userMale->setLat($lat[rand(0, 5)]);
            $userMale->setLng($lng[rand(0, 5)]);
            $userMale->setIsFirstConnection(1);


            $this->directoryLibrary->createDirectoryForNewUserFixtures($userMale);

            $userInfosEntityMale = new UserInfos();
            $userMale->setInfos($userInfosEntityMale);
            $userSliderMale = new Slider();
            $userMale->setSlider($userSliderMale);
            $userMaleRating = new Rating();
            $userMale->setRating($userMaleRating);
            $userMaleMatching = new Matching();
            $userMale->setMatching($userMaleMatching);
            $userMaleFilter = new Filter();
            $userMaleFilter->setRangeMaxDistance(rand(0, 10));
            $userMaleFilter->setRangeAgeMin(18);
            $userMaleFilter->setRangeAgeMax(77);
            $userMale->setFilter($userMaleFilter);

            $userInfosEntityMale->setCity($faker->city);
            $userInfosEntityMale->setAboutyou("BLABLABLABLABLA");
            $userInfosEntityMale->setInterest($interest[rand(0, 5)]);
            $userInfosEntityMale->setLifestyle($lifeStyle[rand(0, 11)]);
            $userInfosEntityMale->setCharacters($characters[rand(0, 10)]);
            $userInfosEntityMale->setOrigin($origin[rand(0, 7)]);
            $userInfosEntityMale->setEyes($eyes[rand(0, 4)]);
            $userInfosEntityMale->setHaircolor($hairCollor[rand(0, 5)]);
            $userInfosEntityMale->setHaircut($hairCut[rand(0, 4)]);
            $userInfosEntityMale->setMorphology($morphhology[rand(0, 7)]);
            $userInfosEntityMale->setIntention($intention[rand(0, 2)]);
            $userInfosEntityMale->setProfileDominance($profileDom[rand(0, 1)]);
            $userInfosEntityMale->setHerefor(UserInfos::SEX_TYPE_WOMEN);
            $userInfosEntityMale->setAlcool($frequency[rand(0, 3)]);
            $userInfosEntityMale->setSmoke($frequency[rand(0, 3)]);
            $userInfosEntityMale->setChildren($closeAnswer[rand(0, 2)]);
            $userInfosEntityMale->setTattoo($closeAnswer[rand(0, 2)]);
            $userInfosEntityMale->setPiercing($closeAnswer[rand(0, 2)]);
            $userInfosEntityMale->setSize(rand(140, 210));
            $userInfosEntityMale->setWeight(rand(40, 210));
            $userInfosEntityMale->setWork('work work');
            $userInfosEntityMale->setSport('sport');
            $userInfosEntityMale->setCountry('FR');

            $manager->persist($userInfosEntityMale);
            $manager->persist($userMale);
        }

        for ($i = 0; $i < 20; $i++) {
            $userFemale = new User();
            $userFemale->setFirstname($faker->firstNameFemale);
            $userFemale->setLastname($faker->lastName);
            $userFemale->setPseudo($faker->firstNameFemale);
            $userFemale->setSex('femme');
            $userFemale->setDob(new \DateTime(rand(1980, 2000) . "-" . rand(1, 12) . "-" . rand(1, 31)));
            $userFemale->setEmail($i . $faker->email);
            $userFemale->setRoles(['ROLE_USER']);
            $userFemale->setPassword($this->passwordEncoder->encodePassword(
                $userFemale,
                'debug0000'
            ));
            $userFemale->setLat($lat[rand(0, 5)]);
            $userFemale->setLng($lng[rand(0, 5)]);
            $userFemale->setIsFirstConnection(1);

            $this->directoryLibrary->createDirectoryForNewUserFixtures($userFemale);

            $userInfosEntityFemale = new UserInfos();
            $userFemale->setInfos($userInfosEntityFemale);
            $userSliderFemale = new Slider();
            $userFemale->setSlider($userSliderFemale);
            $userFemaleRating = new Rating();
            $userFemale->setRating($userFemaleRating);
            $userFemaleMatching = new Matching();
            $userFemale->setMatching($userFemaleMatching);
            $userFemaleFilter = new Filter();
            $userFemaleFilter->setRangeMaxDistance(rand(0, 10));
            $userFemaleFilter->setRangeAgeMin(18);
            $userFemaleFilter->setRangeAgeMax(77);
            $userFemale->setFilter($userFemaleFilter);

            $userInfosEntityFemale->setCity($faker->city);
            $userInfosEntityFemale->setAboutyou("BLUBLUBLUBLU");
            $userInfosEntityFemale->setInterest($interest[rand(0, 5)]);
            $userInfosEntityFemale->setLifestyle($lifeStyle[rand(0, 11)]);
            $userInfosEntityFemale->setCharacters($characters[rand(0, 10)]);
            $userInfosEntityFemale->setOrigin($origin[rand(0, 7)]);
            $userInfosEntityFemale->setEyes($eyes[rand(0, 4)]);
            $userInfosEntityFemale->setHaircolor($hairCollor[rand(0, 5)]);
            $userInfosEntityFemale->setHaircut($hairCut[rand(0, 4)]);
            $userInfosEntityFemale->setMorphology($morphhology[rand(0, 7)]);
            $userInfosEntityFemale->setIntention($intention[rand(0, 2)]);
            $userInfosEntityFemale->setProfileDominance($profileDom[rand(0, 1)]);
            $userInfosEntityFemale->setHerefor(UserInfos::SEX_TYPE_MEN);
            $userInfosEntityFemale->setAlcool($frequency[rand(0, 3)]);
            $userInfosEntityFemale->setSmoke($frequency[rand(0, 3)]);
            $userInfosEntityFemale->setChildren($closeAnswer[rand(0, 2)]);
            $userInfosEntityFemale->setTattoo($closeAnswer[rand(0, 2)]);
            $userInfosEntityFemale->setPiercing($closeAnswer[rand(0, 2)]);
            $userInfosEntityFemale->setSize(rand(140, 210));
            $userInfosEntityFemale->setWeight(rand(40, 210));
            $userInfosEntityFemale->setWork('work work');
            $userInfosEntityFemale->setSport('sport');
            $userInfosEntityFemale->setCountry('FR');

            $manager->persist($userInfosEntityFemale);
            $manager->persist($userFemale);
        }
        $manager->flush();
    }
}
