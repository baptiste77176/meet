<?php

namespace App\Form;

use App\Entity\UserInfos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserInfosComplementaryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', TextType::class, [
                'required' => true,
            ])
            ->add('aboutyou', TextareaType::class, [
                'required' => false,
            ])
            ->add('profileDominance', ChoiceType::class, array(
                'choices' => array(
                    'reckless' => UserInfos::PROFILE_DOM_RECKLESS,
                    'shy' => UserInfos::PROFILE_DOM_SHY,

                ),
                'placeholder' => '',
                'required' => true,
            ))
            ->add('intention', ChoiceType::class, array(
                'choices' => array(
                    'cdi' => UserInfos::INTENTION_CDI,
                    'cdd' => UserInfos::INTENTION_CDD,
                    'both' => UserInfos::INTENTION_BOTH,

                ),
                'placeholder' => '',
                'required' => true,
            ))
            ->add('herefor', ChoiceType::class, array(
                'choices' => array(
                    'homme' => UserInfos::SEX_TYPE_MEN,
                    'femme' => UserInfos::SEX_TYPE_WOMEN,
                ),
                'placeholder' => '',
                'required' => true,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserInfos::class,
        ]);
    }
}
