<?php

namespace App\Form;

use App\Entity\Filter;
use App\Entity\UserInfos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResearchFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rangeAgeMin', RangeType::class, [
                'attr' => [
                    'min' => 18,
                    'max' => 77,
                ]
            ])
            ->add('rangeAgeMax', RangeType::class, [
                'attr' => [
                    'min' => 18,
                    'max' => 77,
                ]
            ])
            ->add('rangeMaxDistance', RangeType::class, [
                'attr' => [
                    'min' => 1,
                    'max' => 15000,
                ]
            ])
            ->add('interest', ChoiceType::class, array(
                'choices' => array(
                    'Cinema' => UserInfos::INTEREST_CINEMA,
                    'Musique' => UserInfos::INTEREST_MUSIC,
                    'Art' => UserInfos::INTEREST_ART,
                    'Cuisine' => UserInfos::INTEREST_COOKING,
                    'Sport' => UserInfos::INTEREST_SPORT,
                    'Littérature' => UserInfos::INTEREST_LITTERATURE,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('lifestyle', ChoiceType::class, array(
                'choices' => array(
                    'Sportif' => UserInfos::LIFESTYLE_SPORT,
                    'Entreprenant' => UserInfos::LIFESTYLE_ENTERPRISING,
                    'Voyageur' => UserInfos::LIFESTYLE_TRAVELLER,
                    'Travailleur' => UserInfos::LIFESTYLE_WORKER,
                    'Ambitieux' => UserInfos::LIFESTYLE_AMBITIOUS,
                    'Root' => UserInfos::LIFESTYLE_ROOT,
                    'Casanier' => UserInfos::LIFESTYLE_HOMEBODY,
                    'Intello' => UserInfos::LIFESTYLE_NERD,
                    'Réveur' => UserInfos::LIFESTYLE_DREAMER,
                    'Fétard' => UserInfos::LIFESTYLE_PARTY,
                    'Classique' => UserInfos::LIFESTYLE_CLASSIC,
                    'Geek' => UserInfos::LIFESTYLE_GEEK,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('characters', ChoiceType::class, array(
                'choices' => array(
                    'Attentionné' => UserInfos::CHARACTERS_ATTENTIVE,
                    'Extravertie' => UserInfos::CHARACTERS_OUTGOING,
                    'social' => UserInfos::CHARACTERS_SOCIAL,
                    'Dynamique' => UserInfos::CHARACTERS_DYNAMIC,
                    'Calme' => UserInfos::CHARACTERS_QUIET,
                    'Indépendant' => UserInfos::CHARACTERS_INDEPENDANT,
                    'Discret' => UserInfos::CHARACTERS_DISCREET,
                    'Malicieux' => UserInfos::CHARACTERS_MALICIOUS,
                    'Exentrique' => UserInfos::CHARACTERS_ECCENTRIC,
                    'Curieux' => UserInfos::CHARACTERS_CURIOUS,
                    'Romantique' => UserInfos::CHARACTERS_ROMANTIC,
                ),
                'placeholder' => '',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
        ]);
    }
}
