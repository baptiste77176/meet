<?php

namespace App\Form;

use App\Entity\UserInfos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserInfosMenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', TextType::class, [
                'required' => true,
            ])
            ->add('intention', ChoiceType::class, array(
                'choices' => array(
                    'cdi' => UserInfos::INTENTION_CDI,
                    'cdd' => UserInfos::INTENTION_CDD,
                    'both' => UserInfos::INTENTION_BOTH,
                ),
                'placeholder' => '',
                'required' => true,
            ))
            ->add('profileDominance', ChoiceType::class, array(
                'choices' => array(
                    'reckless' => UserInfos::PROFILE_DOM_RECKLESS,
                    'shy' => UserInfos::PROFILE_DOM_SHY,
                ),
                'placeholder' => '',
                'required' => true,
            ))
            ->add('aboutyou', TextareaType::class, [
                'required' => false,
            ])
            ->add('country', CountryType::class, [
                'placeholder' => '',
                'required' => false
            ])
            ->add('children', ChoiceType::class, array(
                'choices' => array(
                    'Oui' => UserInfos::YES,
                    'Non' => UserInfos::NO,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('sport', TextType::class, [
                'required' => false,
            ])
            ->add('work', TextType::class, [
                'required' => false,
            ])
            ->add('leisure', TextType::class, [
                'required' => false,
            ])
            ->add('interest', ChoiceType::class, array(
                'choices' => array(
                    'Cinema' => UserInfos::INTEREST_CINEMA,
                    'Musique' => UserInfos::INTEREST_MUSIC,
                    'Art' => UserInfos::INTEREST_ART,
                    'Cuisine' => UserInfos::INTEREST_COOKING,
                    'Sport' => UserInfos::INTEREST_SPORT,
                    'Littérature' => UserInfos::INTEREST_LITTERATURE,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('lifestyle', ChoiceType::class, array(
                'choices' => array(
                    'Sportif' => UserInfos::LIFESTYLE_SPORT,
                    'Entreprenant' => UserInfos::LIFESTYLE_ENTERPRISING,
                    'Voyageur' => UserInfos::LIFESTYLE_TRAVELLER,
                    'Travailleur' => UserInfos::LIFESTYLE_WORKER,
                    'Ambitieux' => UserInfos::LIFESTYLE_AMBITIOUS,
                    'Root' => UserInfos::LIFESTYLE_ROOT,
                    'Casanier' => UserInfos::LIFESTYLE_HOMEBODY,
                    'Intello' => UserInfos::LIFESTYLE_NERD,
                    'Réveur' => UserInfos::LIFESTYLE_DREAMER,
                    'Fétard' => UserInfos::LIFESTYLE_PARTY,
                    'Classique' => UserInfos::LIFESTYLE_CLASSIC,
                    'Geek' => UserInfos::LIFESTYLE_GEEK,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('characters', ChoiceType::class, array(
                'choices' => array(
                    'Attentionné' => UserInfos::CHARACTERS_ATTENTIVE,
                    'Extravertie' => UserInfos::CHARACTERS_OUTGOING,
                    'social' => UserInfos::CHARACTERS_SOCIAL,
                    'Dynamique' => UserInfos::CHARACTERS_DYNAMIC,
                    'Calme' => UserInfos::CHARACTERS_QUIET,
                    'Indépendant' => UserInfos::CHARACTERS_INDEPENDANT,
                    'Discret' => UserInfos::CHARACTERS_DISCREET,
                    'Malicieux' => UserInfos::CHARACTERS_MALICIOUS,
                    'Exentrique' => UserInfos::CHARACTERS_ECCENTRIC,
                    'Curieux' => UserInfos::CHARACTERS_CURIOUS,
                    'Romantique' => UserInfos::CHARACTERS_ROMANTIC,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('origin', ChoiceType::class, array(
                'choices' => array(
                    'Européen' => UserInfos::ORIGIN_EUROPEAN,
                    'Afro' => UserInfos::ORIGIN_AFRO,
                    'Maghrébin' => UserInfos::ORIGIN_MAGHREB,
                    'Asiatiques' => UserInfos::ORIGIN_ASIA,
                    'Métisses' => UserInfos::ORIGIN_METISS,
                    'Eurasiens' => UserInfos::ORIGIN_EURASIA,
                    'Latines' => UserInfos::ORIGIN_LATINA,
                    'Antillaises' => UserInfos::ORIGIN_ANTILLEAN,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('eyes', ChoiceType::class, array(
                'choices' => array(
                    'Bleu' => UserInfos::EYES_BLUE,
                    'Noir' => UserInfos::EYES_BLACK,
                    'Marron' => UserInfos::EYES_BROWN,
                    'Noisette' => UserInfos::EYES_PEANUTS,
                    'Vert' => UserInfos::EYES_GREEN,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('haircolor', ChoiceType::class, array(
                'choices' => array(
                    'Noir' => UserInfos::HAIRCOLOR_BLACK,
                    'Brun' => UserInfos::HAIRCOLOR_BROWN,
                    'Blond' => UserInfos::HAIRCOLOR_BLOND,
                    'Chatain' => UserInfos::HAIRCOLOR_DARK_BLOND,
                    'Blond vénitien' => UserInfos::HAIRCOLOR_RED,
                    'Autres' => UserInfos::HAIRCOLOR_OTHER,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('haircut', ChoiceType::class, array(
                'choices' => array(
                    'Chauve' => UserInfos::HAIRCUT_BALD,
                    'Raser' => UserInfos::HAIRCUT_SHAVEN,
                    'court' => UserInfos::HAIRCUT_SHORT,
                    'Mi-long' => UserInfos::HAIRCUT_HALF_LONG,
                    'Long' => UserInfos::HAIRCUT_LONG,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('tattoo', ChoiceType::class, array(
                'choices' => array(
                    'Oui' => UserInfos::YES,
                    'Non' => UserInfos::NO,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('piercing', ChoiceType::class, array(
                'choices' => array(
                    'Oui' => UserInfos::YES,
                    'Non' => UserInfos::NO,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('size', IntegerType::class, array(
                'attr' => array('min' => 40, 'max' => 210),
                'required' => false,
            ))
            ->add('weight', IntegerType::class, array(
                'attr' => array('min' => 40, 'max' => 200),
                'required' => false,
            ))
            ->add('morphology', ChoiceType::class, array(
                'choices' => array(
                    'svelte' => UserInfos::MORPHOLOGY_SLENDER,
                    'sportive' => UserInfos::MORPHOLOGY_SPORT,
                    'équilibrée' => UserInfos::MORPHOLOGY_BALANCED,
                    'pulpeuse' => UserInfos::MORPHOLOGY_LUSCIOUS,
                    'généreuse' => UserInfos::MORPHOLOGY_GENEROUS,
                    'normal' => UserInfos::MORPHOLOGY_NORMAL,
                    'Fine' => UserInfos::MORPHOLOGY_DELICATE,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('alcool', ChoiceType::class, array(
                'choices' => array(
                    'Jamais' => UserInfos::FREQUENCY_NEVER,
                    'Rarement' => UserInfos::FREQUENCY_RARE,
                    'Occasionnel' => UserInfos::FREQUENCY_OCCASIONALLY,
                    'Souvent' => UserInfos::FREQUENCY_OFTEN,
                ),
                'placeholder' => '',
                'required' => false,
            ))
            ->add('smoke', ChoiceType::class, array(
                'choices' => array(
                    'Jamais' => UserInfos::FREQUENCY_NEVER,
                    'Rarement' => UserInfos::FREQUENCY_RARE,
                    'Occasionnel' => UserInfos::FREQUENCY_OCCASIONALLY,
                    'Souvent' => UserInfos::FREQUENCY_OFTEN,
                )
            , 'placeholder' => '',
                'required' => false,
            ))
            ->add('herefor', ChoiceType::class, array(
                'choices' => array(
                    'homme' => UserInfos::SEX_TYPE_MEN,
                    'femme' => UserInfos::SEX_TYPE_WOMEN,
                ),
                'placeholder' => '',
                'required' => true,
            ))
            ->add('favoriteMoovie', TextType::class, [
                'required' => false,
            ])
            ->add('favoriteSong', TextType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserInfos::class,
        ]);
    }
}
