'use strict';

$(document).ready(function () {

    function currentPosition(position) {
        let lat = position.coords.latitude;
        let lng = position.coords.longitude;
        console.log(lat);
        console.log(lng);
        $.ajax({
            data: {
                'lat': lat,
                'lng': lng,
            },
            dataType: 'json',
            url: '/ajax/geoloc',
            type: 'post',
            success: onSuccess,
            error: function () {
                console.log("DEBUG : ERROR");

            }
        })
    }

    function erreurPosition(error) {
        let info = "Erreur lors de la géolocalisation : ";
        switch (error.code) {
            case error.TIMEOUT:
                info += "Timeout !";
                break;
            case error.PERMISSION_DENIED:
                info += "Vous n’avez pas donné la permission";
                alert('Pour le bon fonctionnement de certaines fonctionnalitées ce site à besoin de connaitre votre position');
                break;
            case error.POSITION_UNAVAILABLE:
                info += "La position n’a pu être déterminée";
                break;
            case error.UNKNOWN_ERROR:
                info += "Erreur inconnue";
                break;
        }
        console.log(info)
    }

    function onSuccess(response) {
        console.log(response);
    }


    if (navigator.geolocation)
        navigator.geolocation.getCurrentPosition(currentPosition, erreurPosition, {
            maximumAge: 600000,
            enableHighAccuracy: true
        });
});